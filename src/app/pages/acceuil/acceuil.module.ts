import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AcceuilComponent } from './acceuil.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [AcceuilComponent]
})
export class AcceuilModule { }
 