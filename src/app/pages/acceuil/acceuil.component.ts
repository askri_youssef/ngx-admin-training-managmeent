import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import { throwError } from 'rxjs/internal/observable/throwError';

@Component({
  selector: 'acceuil',
  templateUrl: './acceuil.component.html',
  styleUrls: ['./acceuil.component.scss']
})

export class AcceuilComponent implements OnInit {

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`+
        `Backend message ${error.error.message}`
      );
    }
    // return an observable with a user-facing error message
    return throwError(
      'Something bad happened; please try again later.');
  };


  restItemsServiceGetRestItems(): any {
    return this.http
      .get<any[]>(this.restItemsUrl)
      .pipe(map(data => data))
      .pipe(catchError(this.handleError));
  }

  restItems: any;

  getRestItem(): any {
    this.restItemsServiceGetRestItems()
      .subscribe(
        restItems => {
          this.restItems = restItems;
          console.log(this.restItems);
        }
      )
  }
  restItemsUrl = "http://localhost:8080/student/auth/?login=youssef&pwd=youssefa";

  constructor(private http: HttpClient) { }

  ngOnInit() {
    this.getRestItem();
  }

}
