/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { APP_BASE_HREF } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { CoreModule } from './@core/core.module';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { ThemeModule } from './@theme/theme.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AcceuilModule } from './pages/acceuil/acceuil.module';
import { NbAuthModule, nbStrategiesFactory } from './auth/auth.module';
import { NbAuthService } from './auth/services/auth.service';
import { NbTokenService } from './auth/services/token/token.service';
import { NbTokenLocalStorage } from './auth/services/token/token-storage';
import { NbPasswordAuthStrategy } from './auth/strategies/password/password-strategy';
import { NbPasswordAuthStrategyOptions, NbAuthStrategyOptions } from './auth';
import { NB_AUTH_OPTIONS } from './auth/auth.options';
import { NbAuthJWTToken } from './auth/services';
/**
 *
 *
 * @export
 * @class AppModule
 */
@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    AppRoutingModule,

    NgbModule.forRoot(),
    ThemeModule.forRoot(),
    CoreModule.forRoot(),
    AcceuilModule,
    NbAuthModule.forRoot({
      strategies: [
        NbPasswordAuthStrategy.setup({
          name: 'email',

        })
      ],
      forms: {},
    }),

  ],
  bootstrap: [AppComponent],
  providers: [
    { provide: APP_BASE_HREF, useValue: '/' },


  ],
})
export class AppModule {
}
